# Disable .la file removal since the .la file is exported via apu-config.
%global __brp_remove_la_files %nil

%define apuver 1

Name: apr-util
Version: 1.6.3
Release: 2
Summary: apr-util provides a number of helpful abstractions on top of APR.
License: ASL 2.0
URL: http://apr.apache.org
Source0: http://www.apache.org/dist/apr/%{name}-%{version}.tar.bz2

BuildRequires: gcc autoconf apr-devel >= 1.6.0 gdbm-devel expat-devel libuuid-devel
BuildRequires: mariadb-connector-c-devel sqlite-devel >= 3.1.0 openldap-devel openssl-devel

Requires: apr-util%{?_isa} = %{version}-%{release}

Recommends: apr-util-openssl%{_isa} = %{version}-%{release}
Recommends: apr-util-bdb%{_isa} = %{version}-%{release}

Obsoletes: apr-util-bdb < %{version}-%{release} apr-util-mysql < %{version}-%{release}
Obsoletes: apr-util-sqlite < %{version}-%{release} apr-util-ldap < %{version}-%{release}
Obsoletes: apr-util-openssl < %{version}-%{release}
Provides: apr-util-bdb = %{version}-%{release} apr-util-mysql = %{version}-%{release}
Provides: apr-util-sqlite = %{version}-%{release} apr-util-ldap = %{version}-%{release}
Provides: apr-util-openssl = %{version}-%{release}

%description
The mission of the Apache Portable Runtime (APR) project is to create and maintain software
libraries that provide a predictable and consistent interface to underlying platform-specific
implementations. The primary goal is to provide an API to which software developers may
code and be assured of predictable if not identical behaviour regardless of the platform on
which their software is built, relieving them of the need to code special-case conditions to
work around or take advantage of platform-specific deficiencies or features.

%package devel
Summary: The development kit of apr-util.
Requires: expat-devel%{?_isa} apr-util%{?_isa} = %{version}-%{release}
Requires: gdbm-devel%{?_isa} openldap-devel%{?_isa} apr-devel%{?_isa} pkgconfig

%description devel
The development kit of apr-util.

%package pgsql
Summary: The PostgreSQL DBD driver of apr-util.
BuildRequires: libpq-devel
Requires: apr-util%{?_isa} = %{version}-%{release}

%description pgsql
The PostgreSQL DBD driver of apr-util.

%package odbc
Summary: The ODBC DBD driver of apr-util.
BuildRequires: unixODBC-devel
Requires: apr-util%{?_isa} = %{version}-%{release}

%description odbc
The ODBC DBD driver of apr-util.

%prep
%autosetup -n %{name}-%{version} -p1

%build
autoheader && autoconf
export ac_cv_ldap_set_rebind_proc_style=three
%configure --disable-static --with-apr=%{_prefix} --includedir=%{_includedir}/apr-%{apuver} \
        --with-ldap=ldap_r --with-gdbm --with-sqlite3 --with-pgsql --with-mysql --with-odbc \
        --with-dbm=gdbm --without-berkeley-db --without-sqlite2 --with-crypto --with-openssl
%make_build

%install
%make_install

mkdir -p %{buildroot}/%{_datadir}/aclocal
install -m 644 build/find_apu.m4 %{buildroot}/%{_datadir}/aclocal

# Unpackaged files; remove the static libaprutil
rm -f %{buildroot}%{_libdir}/aprutil.exp \
      %{buildroot}%{_libdir}/libapr*.a

# And remove the reference to the static libaprutil from the .la
# file.
sed -i '/^old_library/s,libapr.*\.a,,' \
      %{buildroot}%{_libdir}/libapr*.la

# Remove unnecessary exports from dependency_libs
sed -ri '/^dependency_libs/{s,-l(pq|sqlite[0-9]|rt|dl|uuid) ,,g}' \
      %{buildroot}%{_libdir}/libapr*.la

# Trim libtool DSO cruft
rm -f %{buildroot}%{_libdir}/apr-util-%{apuver}/*.*a

%check
# Run the less verbose test suites
export MALLOC_CHECK_=2 MALLOC_PERTURB_=$(($RANDOM % 255 + 1))
cd test
%{make_build} testall
# testall breaks with DBD DSO; ignore
export LD_LIBRARY_PATH=%{buildroot}/%{_libdir}/apr-util-%{apuver}
./testall -v -q

%files
%doc CHANGES NOTICE
%license LICENSE
%{_libdir}/libaprutil-%{apuver}.so.*
%dir %{_libdir}/%{name}-%{apuver}
%{_libdir}/%{name}-%{apuver}/apr_dbm_gdbm*
%{_libdir}/%{name}-%{apuver}/apr_dbd_mysql*
%{_libdir}/%{name}-%{apuver}/apr_dbd_sqlite*
%{_libdir}/%{name}-%{apuver}/apr_ldap*
%{_libdir}/%{name}-%{apuver}/apr_crypto_openssl*

%files devel
%{_bindir}/apu-%{apuver}-config
%{_libdir}/libaprutil-%{apuver}.la
%{_libdir}/libaprutil-%{apuver}.so
%{_includedir}/apr-%{apuver}/*.h
%{_libdir}/pkgconfig/*.pc
%{_datadir}/aclocal/*.m4

%files pgsql
%{_libdir}/%{name}-%{apuver}/apr_dbd_pgsql*

%files odbc
%{_libdir}/%{name}-%{apuver}/apr_dbd_odbc*

%changelog
* Thu Aug 29 2024 Funda Wang <fundawang@yeah.net> - 1.6.3-2
- Disable automatic .la file removal
- cleanup spec

* Sat Feb 3 2024 caixiaomeng <caixiaomeng2@huawei.com> - 1.6.3-1
- update to 1.6.3

* Tue Feb 14 2023 fuanan <fuanan3@h-partners.com> - 1.6.1-14
- Fix CVE-2022-25147

* Thu Jul 28 2022 wuzx<wuzx1226@qq.com> - 1.6.1-13
- add sw64 patch

* Sat Jun 19 2021 panxiaohe <panxiaohe@huawei.com> - 1.6.1-12
- BuildRequires: replace libdb with gdbm
- Add requires gdbm-devel

* Mon Jan 13 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.6.1-11
- Delete useless files.

* Tue Oct 22 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.6.1-10
- optimize spec file.

* Sat Sep 28 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.6.1-9
- Package rebuild.

* Thu Sep 05 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.6.1-8
- Package init.
